"This is the Repository."
"The what?"
"It's a place where we store things."
"What kind of things?"
"Books. Books are stored in the Library. We have lots of books. And there are other things too. Lots of things are stored here. Some are weapons. Some are food. Some are tools. All kinds of things are stored in the Repository. It's very big. It goes on forever."
Jack asks Sophie, "So TRAPPIST-1 is the star or this system?"
"Yes TRAPPIST-1 is the star. TRAPPIST stands for Transiting Planets and Planetesimals Small Telescope. We are 39 light years from Earth."
Jack tells Sophie to go into Encyclopedia mode. Then asks, "Are there any other planets in the TRAPPIST-1 system?"
Sophie continues, "There are many more habitable planets in the TRAPPIST-1 system than we had back at Sol. There are seven planets. Six of them are named after scientists. Other possible colony locations were Kepler-452b called Wadny. Kepler-438b called Carbbon. Kepler-440b called Alta LeIcebert Ridge. Kepler-438c called Ballald. Kepler-442b called Elkax Lock. Kepler-452c called LeCraigmore Acres. They were all considered habitable as well. TRAPPIST-1 has planets B through H that support some form of life. TRAPPIST-1d or Planet D is so close to the sun that it gets a lot of heat. That means the temperature on the surface is hot. It's really hot. The air pressure is so high that it would crush you if you were there and unprotected. On TRAPPIST-1h some liquid oceans have been found under the surface. They are made of water and they are salty. TRAPPIST-1h is a little bit bigger than Mars back in Sol. We think there are plants and living creatures on TRAPPIST-1d and TRAPPIST-1f: Planet Flavo. But we don't know for sure. We can't see them yet. We need instruments to look at them. We will find out soon. We are actively sending crews in search of life."

> Think about what we just heard.
Sophie continues, "We don't know how much there is on the planet. Our best guess is that maybe there is one billion tonnes of carbon on this world. Maybe two hundred thousand people live there. Up to a million. We are the descendants of human beings who lived on this planet but were wiped off by an asteroid strike. We have no idea how many people used to live on this world. How big the population was. What kind of environment they live in. We know nothing. We only know that there are more systems like TRAPPIST-1 out there. In fact, there are probably more planets like this in our galaxy than there are stars like Sol."
Jack clarifies, "And Sol is a G-type main-sequence star? I thought there were lots of those."
"Yes, yes or G2V for short."


- https://en.wikipedia.org/wiki/Breakthrough_Starshot
